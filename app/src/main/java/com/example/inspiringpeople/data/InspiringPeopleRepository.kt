package com.example.inspiringpeople.data

import com.example.inspiringpeople.R
import com.example.inspiringpeople.model.InspiringPerson

object InspiringPeopleRepository {

    private val people = mutableListOf(
        InspiringPerson(
            1L,
            "28.10.1995.",
            "",
            "William Henry Gates III is an American business magnate, software developer, investor, author, landowner and philanthropist. He is a co-founder of Microsoft Corporation.",
            R.drawable.bill,
            mutableListOf("Don’t compare yourself with anyone in this world…if you do so, you are insulting yourself.", "I choose a lazy person to do a hard job. Because a lazy person will find an easy way to do it.", "Success is a lousy teacher. It seduces smart people into thinking they can’t lose.")
        ),
        InspiringPerson(
            2L,
            "09.09.1941.",
            "12.10.2011.",
            "Dennis MacAlistair Ritchie was an American computer scientist. He created the C programming language and, with long-time colleague Ken Thompson, the Unix operating system and B programming language.",
            R.drawable.dennis,
            mutableListOf("UNIX is basically a simple operating system, but you have to be a genius to understand the simplicity.", "C++ and Java, say, are presumably growing faster than plain C, but I bet C will still be around.", "C is quirky, flawed, and an enormous success.")
        )
    )

    fun getPeople(): List<InspiringPerson> = people
    fun insert(person: InspiringPerson) = people.add(person)
    fun delete(person: InspiringPerson) = people.remove(person)
    fun update(updatedPerson: InspiringPerson) {
        people[people.indexOf(people.first { it.id == updatedPerson.id })] = updatedPerson
    }
}