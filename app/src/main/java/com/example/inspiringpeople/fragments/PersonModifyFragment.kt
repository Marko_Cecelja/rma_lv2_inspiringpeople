package com.example.inspiringpeople.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.inspiringpeople.InspiringPeople
import com.example.inspiringpeople.R
import com.example.inspiringpeople.data.InspiringPeopleRepository
import com.example.inspiringpeople.databinding.FragmentPersonModifyBinding
import com.example.inspiringpeople.model.InspiringPerson
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class PersonModifyFragment : Fragment() {

    private lateinit var personModifyBinding: FragmentPersonModifyBinding

    private val inspiringPeopleRepository = InspiringPeopleRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        personModifyBinding = FragmentPersonModifyBinding.inflate(
            inflater,
            container,
            false
        )

        val person = arguments?.getSerializable(KEY_PERSON)

        arguments?.let {
            person as InspiringPerson
            personModifyBinding.etNewPersonDescription.setText(person.description)
            personModifyBinding.etNewPersonDateOfBirth.setText(person.dateOfBirth)
            personModifyBinding.etNewPersonDateOfDeath.setText(person.dateOfDeath)
        }

        personModifyBinding.bNewPersonSave.setOnClickListener { savePerson(person as InspiringPerson?) }

        return personModifyBinding.root
    }

    companion object {
        const val TAG = "Add new person"
        private const val KEY_PERSON = "person"
        fun create(person: InspiringPerson?): PersonModifyFragment {
            if (person == null) {
                return PersonModifyFragment()
            }

            val args = Bundle()
            args.putSerializable(KEY_PERSON, person)
            val fragment = PersonModifyFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private fun savePerson(person: InspiringPerson?) {
        val description = personModifyBinding.etNewPersonDescription.text.toString()
        val dateOfBirth = personModifyBinding.etNewPersonDateOfBirth.text.toString()
        val dateOfDeath = personModifyBinding.etNewPersonDateOfDeath.text.toString()

        if(!isValidDate(dateOfBirth) || !isValidDate(dateOfDeath)) {
            Toast.makeText(
                InspiringPeople.application,
                "Invalid date. Please use dd.MM.yyyy format!", Toast.LENGTH_SHORT
            ).show()
            return
        }

        if (person == null) {
            inspiringPeopleRepository.insert(
                InspiringPerson(
                    (inspiringPeopleRepository.getPeople().size + 1).toLong(),
                    dateOfBirth,
                    dateOfDeath,
                    description,
                    R.drawable.avatar,
                    mutableListOf()
                )
            )
        } else {
            inspiringPeopleRepository.update(
                InspiringPerson(
                    person.id,
                    dateOfBirth,
                    dateOfDeath,
                    description,
                    person.pictureResource,
                    person.quotes
                )
            )
        }

        fragmentManager?.popBackStackImmediate()
    }

    private fun isValidDate(date: String): Boolean {

        if(date.isEmpty()) {
            return true
        }

        val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        dateFormat.isLenient = false
        try {
            dateFormat.parse(date.trim { it <= ' ' })
        } catch (e: ParseException) {
            return false
        }
        return true
    }

}