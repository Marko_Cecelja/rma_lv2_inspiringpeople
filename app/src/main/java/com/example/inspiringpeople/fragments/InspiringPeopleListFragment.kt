package com.example.inspiringpeople.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.inspiringpeople.R
import com.example.inspiringpeople.adapters.InspiringPeopleAdapter
import com.example.inspiringpeople.data.InspiringPeopleRepository
import com.example.inspiringpeople.databinding.FragmentPeopleListBinding
import com.example.inspiringpeople.listener.OnPersonClickListener
import com.example.inspiringpeople.model.InspiringPerson

class InspiringPeopleListFragment : Fragment(), OnPersonClickListener {

    private lateinit var inspiringPeopleListBinding: FragmentPeopleListBinding

    private val inspiringPeopleRepository = InspiringPeopleRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        inspiringPeopleListBinding = FragmentPeopleListBinding.inflate(inflater, container, false)
        inspiringPeopleListBinding.fabAddPeople.setOnClickListener { openAddPersonFragment() }
        setupRecyclerView()
        return inspiringPeopleListBinding.root
    }

    override fun onResume() {
        super.onResume()
        (inspiringPeopleListBinding.rvPeople.adapter as InspiringPeopleAdapter).refreshData(
            inspiringPeopleRepository.getPeople()
        )
    }

    private fun setupRecyclerView() {
        inspiringPeopleListBinding.rvPeople.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        inspiringPeopleListBinding.rvPeople.adapter =
            InspiringPeopleAdapter(inspiringPeopleRepository.getPeople(), this)
    }

    companion object {
        const val TAG = "Inspiring people list"
        fun create(): InspiringPeopleListFragment {
            return InspiringPeopleListFragment()
        }
    }

    private fun openAddPersonFragment() {
        fragmentManager!!.beginTransaction()
            .replace(
                R.id.fl_fragmentContainer,
                PersonModifyFragment.create(null),
                PersonModifyFragment.TAG
            )
            .addToBackStack(TAG)
            .commit()
    }

    override fun deletePerson(person: InspiringPerson) {
        inspiringPeopleRepository.delete(person)
        (inspiringPeopleListBinding.rvPeople.adapter as InspiringPeopleAdapter).refreshData(
            inspiringPeopleRepository.getPeople()
        )
    }

    override fun editPerson(person: InspiringPerson) {
        fragmentManager!!.beginTransaction()
            .replace(
                R.id.fl_fragmentContainer,
                PersonModifyFragment.create(person),
                PersonModifyFragment.TAG
            )
            .addToBackStack(TAG)
            .commit()
    }
}