package com.example.inspiringpeople.listener

import com.example.inspiringpeople.model.InspiringPerson

interface OnPersonClickListener {

    fun deletePerson(person: InspiringPerson)

    fun editPerson(person: InspiringPerson)
}