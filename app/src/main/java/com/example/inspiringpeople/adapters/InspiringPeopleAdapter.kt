package com.example.inspiringpeople.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.inspiringpeople.R
import androidx.recyclerview.widget.RecyclerView
import com.example.inspiringpeople.listener.OnPersonClickListener
import com.example.inspiringpeople.model.InspiringPerson

class InspiringPeopleAdapter(
    people: List<InspiringPerson>, private val onPersonClickListener: OnPersonClickListener
) :
    RecyclerView.Adapter<InspiringPeopleViewHolder>() {

    private val people: MutableList<InspiringPerson> = mutableListOf()
    init {
        refreshData(people)
    }

    fun refreshData(people: List<InspiringPerson>) {
        this.people.clear()
        this.people.addAll(people)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InspiringPeopleViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_person, parent, false)

        return InspiringPeopleViewHolder(view)
    }

    override fun onBindViewHolder(holder: InspiringPeopleViewHolder, position: Int) {
        val person = people[position]
        holder.bind(person, onPersonClickListener)

    }

    override fun getItemCount(): Int {
        return people.size
    }
}