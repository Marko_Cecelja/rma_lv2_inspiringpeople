package com.example.inspiringpeople.adapters

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.inspiringpeople.InspiringPeople
import com.example.inspiringpeople.databinding.ItemPersonBinding
import com.example.inspiringpeople.listener.OnPersonClickListener
import com.example.inspiringpeople.model.InspiringPerson

class InspiringPeopleViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {

    fun bind(person: InspiringPerson, onPersonClickListener: OnPersonClickListener) {
        val itemBinding = ItemPersonBinding.bind(itemView)
        itemBinding.tvPersonItemBirthDeath.text = String.format(
            "%s - %s",
            person.dateOfBirth,
            person.dateOfDeath
        )
        itemBinding.tvPersonItemDescription.text = person.description
        itemBinding.ivPersonItemPicture.setBackgroundResource(person.pictureResource)
        itemBinding.ivPersonItemPicture.setOnClickListener { displayQuote(person) }
        itemBinding.ibPersonRemove.setOnClickListener { onPersonClickListener.deletePerson(person) }
        itemBinding.ibPersonEdit.setOnClickListener { onPersonClickListener.editPerson(person) }
    }

    private fun displayQuote(person: InspiringPerson) {
        if (person.quotes.isNotEmpty()) {
            Toast.makeText(
                InspiringPeople.application,
                person.quotes.random(), Toast.LENGTH_SHORT
            ).show()
        }
    }
}