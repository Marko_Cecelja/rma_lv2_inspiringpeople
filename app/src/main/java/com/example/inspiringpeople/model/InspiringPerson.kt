package com.example.inspiringpeople.model

import java.io.Serializable

data class InspiringPerson(
    val id: Long,
    var dateOfBirth: String,
    var dateOfDeath: String,
    var description: String,
    val pictureResource: Int,
    val quotes: MutableList<String>
) : Serializable