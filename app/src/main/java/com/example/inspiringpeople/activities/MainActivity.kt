package com.example.inspiringpeople.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.inspiringpeople.R
import com.example.inspiringpeople.databinding.ActivityMainBinding
import com.example.inspiringpeople.fragments.InspiringPeopleListFragment

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction()
                .add(R.id.fl_fragmentContainer, InspiringPeopleListFragment.create(), InspiringPeopleListFragment.TAG)
                .commit()
        }
    }
}